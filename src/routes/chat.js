import express from 'express';
import Message from '../model/Message';
import config from '../config';

const router = express.Router();

router.get('/messages', async (req, res) => {

    let messages = [];
    try {
        messages = await Message
            .find({
                date: {$lt: req.query.key ? new Date(req.query.key).getTime() : Date.now()}
            })
            .sort('-date')
            .limit(config.messagesPerPage + 1)
    } catch (error) {
        console.log(error);
        res.status(500);
    }

    let key;
    if (messages.length >= config.messagesPerPage) {
        messages.splice(messages.length - 1, 1);
        key = messages[messages.length - 1].date;
    }

    res.json({messages, key});
});

export default router;
